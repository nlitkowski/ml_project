import pandas as pd
import os
from sklearn.linear_model import LinearRegression, SGDRegressor
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import mean_squared_error, mean_absolute_error
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.layers.experimental import preprocessing

# CONFIG ########################

IN_FILE_NAME = "in.tsv"
OUT_LINEAR_FILE_NAME = "out_linear.tsv"
OUT_KNN_FILE_NAME = "out_knn.tsv"
OUT_SGD_FILE_NAME = "out_sgd.tsv"
OUT_MB_SGD_FILE_NAME = "out_mini_batch_sgd.tsv"
OUT_TF_NN_FILE_NAME = "out_tf_nn.tsv"
MAIN_DIR = "."
HEADERS_FILE_NAME = "headers.tsv"
TRAIN_DIR = "train"
TRAIN_FILE_NAME = "train.tsv"
VALUE_SEP = "\t"
LINE_SEP = "\n"
CATEGORY_TYPE = "category"
DATA_DIR = "data"
DEV_DIR = "dev"
EXP_FILE_NAME = "expected.tsv"

MB_BATCH_SIZE = 50

#################################


def main():
    names = get_names()

    dev_dir_joint = os.path.join(MAIN_DIR, DATA_DIR, DEV_DIR)
    dev_df = get_input_data(dev_dir_joint, names)

    print(f"Dev data size: {len(dev_df)}")

    X, Y = get_train_data(names)

    exp_df = pd.read_csv(os.path.join(
        MAIN_DIR, DATA_DIR, DEV_DIR, EXP_FILE_NAME), header=None, sep=VALUE_SEP, names=[names[0]])

    predict_mini_batch_sgd(X, Y, exp_df, dev_df, dev_dir_joint)
    predict_sgd(X, Y, exp_df, dev_df, dev_dir_joint)
    predict_knn(X, Y, exp_df, dev_df, dev_dir_joint)
    predict_linear(X, Y, exp_df, dev_df, dev_dir_joint)
    predict_torch_nn(X, Y, exp_df, dev_df, dev_dir_joint)


def predict_linear(X, Y, exp_df, dev_df, dev_dir):
    clf = LinearRegression().fit(X, Y)

    out_path = os.path.join(
        dev_dir, OUT_LINEAR_FILE_NAME)
    p = clf.predict(dev_df)
    p.tofile(out_path, sep=LINE_SEP)
    print(f"Linear regression predictions saved to file: {out_path}")
    print(
        f"Linear regression RMSE: {round(mean_squared_error(exp_df, p, squared=False), 2)}")
    print(
        f"Linear regression MAE: {round(mean_absolute_error(exp_df, p), 2)}")


def predict_knn(X, Y, exp_df, dev_df, dev_dir):
    clf = KNeighborsClassifier().fit(X, Y)

    out_path = os.path.join(
        dev_dir, OUT_KNN_FILE_NAME)
    p = clf.predict(dev_df)
    p.tofile(out_path, sep=LINE_SEP)
    print(f"KNN predictions saved to file: {out_path}")
    print(
        f"KNN RMSE: {round(mean_squared_error(exp_df, p, squared=False), 2)}")
    print(
        f"KNN MAE: {round(mean_absolute_error(exp_df, p), 2)}")


def predict_sgd(X, Y, exp_df, dev_df, dev_dir):
    out_path = os.path.join(dev_dir, OUT_SGD_FILE_NAME)

    model = make_pipeline(StandardScaler(), SGDRegressor())
    model.fit(X, Y)

    y_predicted = model.predict(dev_df)

    print(
        f"SGD RMSE: {round(mean_squared_error(exp_df, y_predicted, squared=False), 2)}")
    print(
        f"SGD MAE: {round(mean_absolute_error(exp_df, y_predicted), 2)}")


def batch_iterate(x, y, batch_size):
    assert len(x) == len(y)
    dataset_size = len(x)
    current_index = 0
    while current_index < dataset_size:
        x_batch = x[current_index: current_index + batch_size]
        y_batch = y[current_index: current_index + batch_size]
        yield x_batch, y_batch
        current_index += batch_size


def predict_mini_batch_sgd(X, Y, exp_df, dev_df, dev_dir):
    out_path = os.path.join(dev_dir, OUT_MB_SGD_FILE_NAME)

    scaler = StandardScaler()
    x_train_scaled = scaler.fit_transform(X)

    model = SGDRegressor()
    batch_iterator = batch_iterate(x_train_scaled, Y, batch_size=MB_BATCH_SIZE)
    for x_batch, y_batch in batch_iterator:
        model.partial_fit(x_batch, y_batch)

    y_predicted = model.predict(dev_df)

    print(
        f"Mini-batch SGD RMSE: {round(mean_squared_error(exp_df, y_predicted, squared=False), 2)}")
    print(
        f"Mini-batch SGD MAE: {round(mean_absolute_error(exp_df, y_predicted), 2)}")


def build_and_compile_model(norm):
    model = keras.Sequential([
        norm,
        layers.Dense(64, activation='relu'),
        layers.Dense(64, activation='relu'),
        layers.Dense(1)
    ])

    model.compile(loss='mean_absolute_error',
                  optimizer=tf.keras.optimizers.Adam(0.001))
    return model


def predict_torch_nn(X, Y, exp_df, dev_df, dev_dir):
    out_path = os.path.join(dev_dir, OUT_TF_NN_FILE_NAME)
    normalizer = preprocessing.Normalization(axis=-1)
    normalizer.adapt(np.array(X))
    dnn_model = build_and_compile_model(normalizer)
    _ = dnn_model.fit(
        X, Y,
        validation_split=0.2,
        verbose=0, epochs=100)
    test_predictions = dnn_model.predict(dev_df).flatten()
    print(
        f"DNN RMSE: {round(mean_squared_error(exp_df, test_predictions, squared=False), 2)}")
    print(
        f"DNN MAE: {round(mean_absolute_error(exp_df, test_predictions), 2)}")


def get_train_data(names: list):
    train_path = os.path.join(MAIN_DIR, DATA_DIR, TRAIN_DIR, TRAIN_FILE_NAME)
    check_file(train_path)
    train_data = process_input(pd.read_csv(
        train_path, header=None, sep=VALUE_SEP, names=names), names)

    train_data = clean_data(train_data, names)

    # split data to what is predicted (price)
    # and what is used to predict the price
    X = train_data.loc[:, train_data.columns != names[0]]
    Y = train_data[names[0]]

    return X, Y


def clean_data(data, names):
    # remove the outliers with price less than 1000
    print(f"Train data size: {len(data)}")
    data = data.loc[(data[names[0]] > 1000)]
    print(f"Cleaned train data size: {len(data)}")
    return data


def get_input_data(dirname, names):
    in_path = os.path.join(dirname, IN_FILE_NAME)
    check_file(in_path)
    return process_input(pd.read_csv(in_path, header=None, sep=VALUE_SEP, names=names[1:]), names)


def process_input(df, names):
    df = df.drop([names[4]], axis=1)
    for c in df.select_dtypes(include=object).columns.values:
        df[c] = df[c].astype(CATEGORY_TYPE).cat.codes
    return df


def get_names() -> list:
    names_path = os.path.join(MAIN_DIR, DATA_DIR, HEADERS_FILE_NAME)
    check_file(names_path)

    with open(names_path) as f_names:
        return f_names.read().strip().split(VALUE_SEP)


def check_file(filename: str):
    if not os.path.exists(filename):
        raise FileNotFoundError(filename)


if __name__ == "__main__":
    main()
